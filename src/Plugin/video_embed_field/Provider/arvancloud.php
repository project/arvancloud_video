<?php

namespace Drupal\video_embed_arvancloud\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;
use Drupal\Component\Serialization;
use  Drupal\Core\Config;
/**
 * @VideoEmbedProvider(
 *   id = "arvancloud",
 *   title = @Translation("arvancloud")
 * )
 */
class arvancloud extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    // @todo, consider using the JavaScript version, however iframes are less
    // impact to page load and also don't grant JS access to your website to
    // arvancloud.
    $Authorization=\Drupal::config('video_embed_arvancloud.settings')->get('video_embed_arvancloud_API_Key');
    $video_id=$this->getVideoId();
    $vod_url='https://napi.arvancloud.com/vod/2.0/videos/'.$video_id;

    $headers = array('Authorization'=>$Authorization);
    $options = ['headers' => $headers , 'body' => '','http_errors' => FALSE];
    $client = \Drupal::httpClient();
    try {
      $response=$client->get($vod_url,$options);
    }
    catch (RequestException $error) {
      \Drupal::messenger()->addMessage($error, 'error');
    }
    $StatusCode=$response->getStatusCode();
    if ( $StatusCode== 401) {
      \Drupal::messenger()->addMessage('Authorization Error; Check your API key', 'error');
    }
    elseif ($StatusCode== 200){
      $json= json_decode($response->getBody()->getContents(), TRUE);
      $vod_url=$json['data']['player_url'];
    }

    return [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
        'src' => sprintf( $vod_url, $autoplay),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    return sprintf('', $this->getVideoId());
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
   return $input;
  }

}
