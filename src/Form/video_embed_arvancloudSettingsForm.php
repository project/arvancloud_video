<?php
namespace Drupal\video_embed_arvancloud\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class video_embed_arvancloudSettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'video_embed_arvancloud.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'video_embed_arvancloud_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['video_embed_arvancloud_API_Key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('video_embed_arvancloud_API_Key'),
    ];  

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('video_embed_arvancloud_API_Key', $form_state->getValue('video_embed_arvancloud_API_Key'))
      // You can set multiple configurations at once by making
      // multiple calls to set().
      ->save();

    parent::submitForm($form, $form_state);
  }

}
